package meli.com.checkoutmp.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diegoiribarne on 10/6/16.
 */
public class MockApi implements ServiceApi {
    private static MockApi ourInstance = new MockApi();

    public static MockApi getInstance() {
        return ourInstance;
    }

    private MockApi() {
    }

    @Override
    public void get_payment_method(Callback<List<CreditCard>> callback) {
        List<CreditCard> card = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            card.add(generateRandomCreditCard());
        }
        callback.onResponse(null, Response.success(card));
    }

    private CreditCard generateRandomCreditCard() {
        CreditCard c = new CreditCard(UUID.randomUUID().toString(), "Tarjeta nº" + new Random().nextInt(100), "credit_card", "broken", new Random().nextInt(250000));
        return c;
    }

    private CardIssuers generateRandomCardIssuers() {
        CardIssuers c = new CardIssuers(UUID.randomUUID().toString(), "Banco nº" + new Random().nextInt(100), "broken");
        return c;
    }

    private Payer generateRandomPayer() {
        Integer installment = new Random().nextInt(12) + 1;
        Payer p = new Payer("1000 en " + installment + "cuotas", installment);
        return p;

    }

    @Override
    public void get_card_issuers(Callback<List<CardIssuers>> callBack, String paymentId) {
        List<CardIssuers> card = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            card.add(generateRandomCardIssuers());
        }
        callBack.onResponse(null, Response.success(card));
    }

    @Override
    public void get_installments(Callback<List<PaymentInstallments>> callback, String paymentId, Integer price, String issuerId) {
        List<Payer> payers = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            payers.add(generateRandomPayer());
        }
        List<PaymentInstallments> list = new ArrayList<>();
        list.add(new PaymentInstallments(payers));
        callback.onResponse(null, Response.success(list));
    }
}
