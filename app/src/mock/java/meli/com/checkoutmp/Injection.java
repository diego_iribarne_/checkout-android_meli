package meli.com.checkoutmp;

import meli.com.checkoutmp.data.ApiMercadoPago;
import meli.com.checkoutmp.data.MockApi;
import meli.com.checkoutmp.data.ServiceApi;

public class Injection {
    public static ServiceApi provideServiceApi() {
        return MockApi.getInstance();
    }
}