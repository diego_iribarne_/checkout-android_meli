package meli.com.checkoutmp.issuerslist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import meli.com.checkoutmp.R;
import meli.com.checkoutmp.methodlist.MethodListFragment;

public class IssuersListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, IssuersListFragment.newInstance()).commit();

    }
}
