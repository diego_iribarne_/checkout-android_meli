package meli.com.checkoutmp.issuerslist;

import java.util.ArrayList;
import java.util.List;

import meli.com.checkoutmp.data.CardIssuers;
import meli.com.checkoutmp.data.CheckoutForm;
import meli.com.checkoutmp.data.CreditCard;
import meli.com.checkoutmp.data.ServiceApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public class IssuersListPresenter implements IssuersListContract.UserActionsListener {
    ServiceApi api;
    IssuersListContract.View userView;

    public IssuersListPresenter(ServiceApi api, IssuersListContract.View userView) {
        this.api = api;
        this.userView = userView;
    }


    @Override
    public void loadIssuers(String id) {
        api.get_card_issuers(new Callback<List<CardIssuers>>() {
            @Override
            public void onResponse(Call<List<CardIssuers>> call, Response<List<CardIssuers>> response) {
                userView.showProgress(false);
                userView.showIssuers(response.body());
            }

            @Override
            public void onFailure(Call<List<CardIssuers>> call, Throwable t) {

            }
        }, id);
    }

    @Override
    public void clickIssuersItem(String id) {
        CheckoutForm.getInstance().setIssuer_id(id);
        userView.opeInstallmentList();
    }


    public List<CreditCard> filterCreditCardType(List<CreditCard> list) {
        List<CreditCard> onlyCreditList = new ArrayList<>();
        for (CreditCard card : list) {
            if (card.isCreditCard())
                onlyCreditList.add(card);
        }
        return onlyCreditList;
    }
}
