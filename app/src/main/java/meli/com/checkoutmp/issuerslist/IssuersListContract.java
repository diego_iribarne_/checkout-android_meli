package meli.com.checkoutmp.issuerslist;

import java.util.List;

import meli.com.checkoutmp.data.CardIssuers;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public interface IssuersListContract {
    interface View {
        void showIssuers(List<CardIssuers> method);

        void showProgress(Boolean show);

        void opeInstallmentList();

    }

    interface UserActionsListener {

        void loadIssuers(String id);

        void clickIssuersItem(String id);

    }
}