package meli.com.checkoutmp.data;

public class Payer {
    String recommended_message;
    Integer installments;

    public String getRecommended_message() {
        return recommended_message;
    }

    public Integer getInstallments() {
        return installments;
    }

    public Payer(String recommended_message, Integer installments) {
        this.recommended_message = recommended_message;
        this.installments = installments;
    }
}
