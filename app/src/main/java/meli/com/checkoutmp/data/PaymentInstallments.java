package meli.com.checkoutmp.data;

import java.util.List;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public class PaymentInstallments {
    List<Payer> payer_costs;

    public PaymentInstallments(List<Payer> payer_costs) {
        this.payer_costs = payer_costs;
    }

    public List<Payer> getPayer_costs() {
        return payer_costs;
    }
}

