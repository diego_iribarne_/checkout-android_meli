package meli.com.checkoutmp.data;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public class CreditCard {
    String id;
    String name;
    String payment_type_id;
    String thumbnail;
    Integer max_allowed_amount;

    private final String CREDIT_CARD_TYPE = "credit_card";
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public CreditCard(String id, String name, String payment_type_id, String thumbnail, Integer max_allowed_amount) {
        this.id = id;
        this.name = name;
        this.payment_type_id = payment_type_id;
        this.thumbnail = thumbnail;
        this.max_allowed_amount = max_allowed_amount;
    }

    public String getPayment_type_id() {
        return payment_type_id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public Integer getMax_allowed_amount() {
        return max_allowed_amount;
    }

    public boolean isCreditCard() {
        return payment_type_id.equals(CREDIT_CARD_TYPE);
    }
}
