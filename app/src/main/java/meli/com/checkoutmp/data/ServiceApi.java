package meli.com.checkoutmp.data;

import java.util.List;

import retrofit2.Callback;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public interface ServiceApi {
    void get_payment_method(Callback<List<CreditCard>> callback);
    void get_card_issuers(Callback<List<CardIssuers>> callBack, String paymentId);
    void get_installments(Callback<List<PaymentInstallments>> callback,String paymentId,Integer price,String issuerId);
}
