package meli.com.checkoutmp.data;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public class CheckoutForm {

    private static Integer price = 0;
    private static String payment_method_id;
    private static String issuer_id;
    private static Integer installments;
    private static CheckoutForm ourInstance = new CheckoutForm();

    public static CheckoutForm getInstance() {
        return ourInstance;
    }

    private CheckoutForm() {
    }

    public void setPayment_method_id(String payment_method_id) {
        this.payment_method_id = payment_method_id;
    }

    public void setIssuer_id(String issuer_id) {
        this.issuer_id = issuer_id;
    }

    public static void setOurInstance(CheckoutForm ourInstance) {
        CheckoutForm.ourInstance = ourInstance;
    }

    public static Integer getInstallments() {
        return installments;
    }

    public static void setInstallments(Integer installments) {
        CheckoutForm.installments = installments;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPrice() {
        return price;
    }

    public String getPayment_method_id() {
        return payment_method_id;
    }


    public String getIssuer_id() {
        return issuer_id;
    }

    public static CheckoutForm getOurInstance() {
        return ourInstance;
    }

}
