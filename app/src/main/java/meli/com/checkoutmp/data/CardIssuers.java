package meli.com.checkoutmp.data;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public class CardIssuers {
    String id;
    String name;
    String thumbnail;

    public CardIssuers(String id, String name, String thumbnail) {
        this.id = id;
        this.name = name;
        this.thumbnail = thumbnail;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}
