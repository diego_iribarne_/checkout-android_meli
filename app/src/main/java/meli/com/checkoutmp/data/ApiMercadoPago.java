package meli.com.checkoutmp.data;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public class ApiMercadoPago implements ServiceApi {

    public static final String API_URL = "https://api.mercadopago.com";
    public static final String VERSION = "/v1/";
    public static final String KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";

    private static ApiMercadoPago ourInstance = new ApiMercadoPago();
    private final Retrofit retrofit;
    private final MercadoPago mercadopago;

    public static ApiMercadoPago getInstance() {
        return ourInstance;
    }

    private ApiMercadoPago() {
        retrofit = new Retrofit.Builder()
                .baseUrl(API_URL + VERSION).addConverterFactory(GsonConverterFactory.create())
                .build();
        // Create an instance of our GitHub API interface.
        mercadopago = retrofit.create(MercadoPago.class);
    }

    @Override
    public void get_payment_method(Callback<List<CreditCard>> callback) {
        mercadopago.payment(KEY).enqueue(callback);
    }

    @Override
    public void get_card_issuers(Callback<List<CardIssuers>> callBack, String paymentId) {
        mercadopago.cardissuers(KEY, paymentId).enqueue(callBack);
    }

    @Override
    public void get_installments(Callback<List<PaymentInstallments>> callback, String paymentId, Integer price, String issuerId) {
        mercadopago.installments(KEY, price, paymentId, issuerId).enqueue(callback);
    }

    public interface MercadoPago {
        @GET("payment_methods")
        Call<List<CreditCard>> payment(@Query("public_key") String key);

        @GET("payment_methods/card_issuers")
        Call<List<CardIssuers>> cardissuers(@Query("public_key") String key, @Query("payment_method_id") String paymentID);

        @GET("payment_methods/installments")
        Call<List<PaymentInstallments>> installments(@Query("public_key") String key, @Query("amount") Integer amount,
                                                     @Query("payment_method_id") String paymentID, @Query("issuer.id") String issuerID);
    }
}
