package meli.com.checkoutmp.home;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import meli.com.checkoutmp.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getString(R.string.title_amount));
        getSupportFragmentManager().beginTransaction().replace(R.id.container, HomeFragment.newInstance()).commit();
    }
}
