package meli.com.checkoutmp.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import meli.com.checkoutmp.R;
import meli.com.checkoutmp.data.CheckoutForm;
import meli.com.checkoutmp.methodlist.MethodListActivity;

public class HomeFragment extends Fragment implements HomeContract.View {
    private static final int PRICE_LIMIT = 25000;
    public static final String SHOW_DIALOG = "dialog";
    EditText editText;
    HomeContract.UserActionsListener userActionsListener;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        editText = (EditText) v.findViewById(R.id.priceEditText);
        if (CheckoutForm.getInstance().getPrice() != 0)
            editText.setText("$" + CheckoutForm.getInstance().getPrice());
        final Button goButton = (Button) v.findViewById(R.id.goButton);
        editText.setCursorVisible(false);
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setSelection(editText.getText().length());
                Animation expand = AnimationUtils.loadAnimation(getContext(), R.anim.expand);
                editText.startAnimation(expand);
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                                String cleanString = cleanSymbol(editable);
                                                editText.removeTextChangedListener(this);
                                                if (!TextUtils.isEmpty(cleanString) && Integer.parseInt(cleanString) >= PRICE_LIMIT) {
                                                    editText.setText("$" + PRICE_LIMIT);
                                                } else if (cleanString.equals("0") || cleanString.equals("")) {
                                                    editText.setText("");
                                                } else {
                                                    editText.setText("$" + cleanString);
                                                }
                                                editText.addTextChangedListener(this);
                                                editText.setSelection(editText.getText().length());
                                            }
                                        }

        );
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    goButton.performClick();
                    return true;
                }
                return false;
            }
        });
        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cleanString = cleanSymbol(editText.getText());
                if (TextUtils.isEmpty(cleanString))
                    editText.performClick();
                else {
                    Integer currentPrice = Integer.parseInt(cleanSymbol(editText.getText()));
                    userActionsListener.clickButton(currentPrice);
                }
            }
        });
        userActionsListener = new HomePresenter(this);
        if (getActivity().getIntent().getBooleanExtra(SHOW_DIALOG, false))
            showDialog(CheckoutForm.getInstance());
        return v;
    }

    public String cleanSymbol(Editable edit) {
        return edit.toString().replace("$", "");
    }

    @Override
    public void showMethodList() {
        startActivity(new Intent(getContext(), MethodListActivity.class));
    }

    @Override
    public void showDialog(CheckoutForm form) {
        String content = String.format(getString(R.string.show_select_value), form.getPrice(), form.getPayment_method_id(), form.getIssuer_id(), form.getInstallments());
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.dialog_title)
                .setMessage(content).show();
    }
}
