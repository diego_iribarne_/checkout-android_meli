package meli.com.checkoutmp.home;

import meli.com.checkoutmp.data.CheckoutForm;

/**
 * Created by diegoiribarne on 10/6/16.
 */
public class HomePresenter implements HomeContract.UserActionsListener {
    HomeContract.View userView;

    public HomePresenter(HomeContract.View userView) {
        this.userView = userView;
    }

    @Override
    public void clickButton(Integer price) {
        CheckoutForm.getInstance().setPrice(price);
        userView.showMethodList();
    }
}
