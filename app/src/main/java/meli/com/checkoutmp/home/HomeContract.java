package meli.com.checkoutmp.home;

import meli.com.checkoutmp.data.CheckoutForm;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public interface HomeContract {
    interface View {
        void showMethodList();

        void showDialog(CheckoutForm form);
    }

    interface UserActionsListener {
        void clickButton(Integer price);
    }
}
