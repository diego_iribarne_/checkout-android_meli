package meli.com.checkoutmp.installmentslist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import meli.com.checkoutmp.Injection;
import meli.com.checkoutmp.R;
import meli.com.checkoutmp.data.CheckoutForm;
import meli.com.checkoutmp.data.Payer;
import meli.com.checkoutmp.home.HomeActivity;
import meli.com.checkoutmp.home.HomeFragment;

public class InstallmentsListFragment extends Fragment implements InstallmentsContract.View {
    RecyclerView list;
    ProgressBar progress;
    InstallmentsContract.UserActionsListener userActionsListener;
    private PayersAdapter adapter;

    public InstallmentsListFragment() {
    }

    public static InstallmentsListFragment newInstance() {
        InstallmentsListFragment fragment = new InstallmentsListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.generic_list, container, false);
        list = (RecyclerView) view.findViewById(R.id.list);
        progress = (ProgressBar) view.findViewById(R.id.progress);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new PayersAdapter(new ArrayList<Payer>());
        list.setAdapter(adapter);
        userActionsListener = new InstallmentsPresenter(Injection.provideServiceApi(), this);
        userActionsListener.loadPayers(CheckoutForm.getInstance().getPayment_method_id(), CheckoutForm.getInstance().getPrice(), CheckoutForm.getInstance().getIssuer_id());

        return view;
    }


    @Override
    public void shownInstallments(List<Payer> payers) {
        adapter.setData(payers);
    }

    @Override
    public void showProgress(Boolean show) {
        progress.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showHomeWithDialog() {
        Intent intent = new Intent(getContext(), HomeActivity.class);
        intent.putExtra(HomeFragment.SHOW_DIALOG, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }


    private class PayersAdapter extends RecyclerView.Adapter<PayersAdapter.ViewHolder> {
        List<Payer> data;

        public PayersAdapter(List<Payer> data) {
            this.data = data;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.method_list_item, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final Payer dataItem = data.get(position);
            holder.title.setText(dataItem.getRecommended_message());
            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userActionsListener.clickInstallmentItem(dataItem.getInstallments());
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public void setData(List<Payer> data) {
            this.data = data;
            notifyDataSetChanged();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView title;
            View container;

            public ViewHolder(View itemView) {
                super(itemView);
                container = itemView;
                title = (TextView) itemView.findViewById(R.id.title);
            }
        }
    }
}
