package meli.com.checkoutmp.installmentslist;

import java.util.List;

import meli.com.checkoutmp.data.CheckoutForm;
import meli.com.checkoutmp.data.PaymentInstallments;
import meli.com.checkoutmp.data.ServiceApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diegoiribarne on 10/6/16.
 */
public class InstallmentsPresenter implements InstallmentsContract.UserActionsListener {
    ServiceApi api;
    InstallmentsContract.View userView;

    public InstallmentsPresenter(ServiceApi serviceApi, InstallmentsContract.View userView) {
        this.api = serviceApi;
        this.userView = userView;
    }

    @Override
    public void clickInstallmentItem(Integer installment) {
        CheckoutForm.getInstance().setInstallments(installment);
        userView.showHomeWithDialog();
    }

    @Override
    public void loadPayers(String paymentId, Integer price, String issuerId) {
        api.get_installments(new Callback<List<PaymentInstallments>>() {
            @Override
            public void onResponse(Call<List<PaymentInstallments>> call, Response<List<PaymentInstallments>> response) {
                userView.shownInstallments(response.body().get(0).getPayer_costs());
                userView.showProgress(false);
            }

            @Override
            public void onFailure(Call<List<PaymentInstallments>> call, Throwable t) {

            }
        }, paymentId, price, issuerId);
    }
}
