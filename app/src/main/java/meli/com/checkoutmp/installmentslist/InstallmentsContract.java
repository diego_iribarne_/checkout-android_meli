package meli.com.checkoutmp.installmentslist;

import java.util.List;

import meli.com.checkoutmp.data.Payer;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public interface InstallmentsContract {
    interface View {
        void shownInstallments(List<Payer> payers);

        void showProgress(Boolean show);

        void showHomeWithDialog();
    }

    interface UserActionsListener {
        void clickInstallmentItem(Integer installment);

        void loadPayers(String paymentId, Integer price, String issuerId);

    }
}