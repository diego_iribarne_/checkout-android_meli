package meli.com.checkoutmp.methodlist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import meli.com.checkoutmp.R;

public class MethodListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, MethodListFragment.newInstance()).commit();
    }
}
