package meli.com.checkoutmp.methodlist;

import java.util.List;

import meli.com.checkoutmp.data.CreditCard;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public interface MethodListContract {
    interface View {
        void showMethod(List<CreditCard> method);

        void showProgress(Boolean show);

        void openCardIssuersList();

        void openInstallmentList();
    }

    interface UserActionsListener {
        void openNextPage(String id);
        void loadMethod();

    }
}