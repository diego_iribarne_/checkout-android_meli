package meli.com.checkoutmp.methodlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import meli.com.checkoutmp.Injection;
import meli.com.checkoutmp.R;
import meli.com.checkoutmp.data.CreditCard;
import meli.com.checkoutmp.installmentslist.InstallmentsListActivity;
import meli.com.checkoutmp.issuerslist.IssuersListActivity;

public class MethodListFragment extends Fragment implements MethodListContract.View {
    RecyclerView list;
    ProgressBar progress;
    MethodListContract.UserActionsListener userActionsListener;
    private MethodAdapter adapter;

    public MethodListFragment() {
    }

    public static MethodListFragment newInstance() {
        MethodListFragment fragment = new MethodListFragment();
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.generic_list, container, false);
        list = (RecyclerView) view.findViewById(R.id.list);
        progress = (ProgressBar) view.findViewById(R.id.progress);
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MethodAdapter(new ArrayList<CreditCard>());
        list.setAdapter(adapter);
        userActionsListener = new MethodListPresenter(Injection.provideServiceApi(), this);
        userActionsListener.loadMethod();
        return view;
    }

    @Override
    public void showMethod(List<CreditCard> method) {
        adapter.setData(method);

    }

    @Override
    public void showProgress(Boolean show) {
        progress.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void openCardIssuersList() {
        startActivity(new Intent(getContext(), IssuersListActivity.class));

    }

    @Override
    public void openInstallmentList() {
        startActivity(new Intent(getContext(), InstallmentsListActivity.class));
    }

    private class MethodAdapter extends RecyclerView.Adapter<MethodAdapter.ViewHolder> {
        List<CreditCard> data;

        public MethodAdapter(List<CreditCard> data) {
            this.data = data;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.method_list_item, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final CreditCard dataItem = data.get(position);
            holder.title.setText(dataItem.getName());
            Glide.with(getActivity()).load(dataItem.getThumbnail()).into(holder.image);
            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userActionsListener.openNextPage(dataItem.getId());
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public void setData(List<CreditCard> data) {
            this.data = data;
            notifyDataSetChanged();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView title;
            ImageView image;
            View container;

            public ViewHolder(View itemView) {
                super(itemView);
                container = itemView;
                title = (TextView) itemView.findViewById(R.id.title);
                image = (ImageView) itemView.findViewById(R.id.img);
            }
        }
    }
}
