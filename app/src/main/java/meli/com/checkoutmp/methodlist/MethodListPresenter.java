package meli.com.checkoutmp.methodlist;

import java.util.ArrayList;
import java.util.List;

import meli.com.checkoutmp.data.CardIssuers;
import meli.com.checkoutmp.data.CheckoutForm;
import meli.com.checkoutmp.data.CreditCard;
import meli.com.checkoutmp.data.ServiceApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public class MethodListPresenter implements MethodListContract.UserActionsListener {
    ServiceApi api;
    MethodListContract.View userView;

    public MethodListPresenter(ServiceApi api, MethodListContract.View userView) {
        this.api = api;
        this.userView = userView;
    }

    @Override
    public void openNextPage(String id) {
        CheckoutForm.getInstance().setPayment_method_id(id);
        api.get_card_issuers(new Callback<List<CardIssuers>>() {
            @Override
            public void onResponse(Call<List<CardIssuers>> call, Response<List<CardIssuers>> response) {
                if (response.body().size() > 0)
                    userView.openCardIssuersList();
                else {
                    CheckoutForm.getInstance().setIssuer_id("");
                    userView.openInstallmentList();
                }
            }

            @Override
            public void onFailure(Call<List<CardIssuers>> call, Throwable t) {

            }
        }, id);
    }

    @Override
    public void loadMethod() {
        api.get_payment_method(new Callback<List<CreditCard>>() {
            @Override
            public void onResponse(Call<List<CreditCard>> call, Response<List<CreditCard>> response) {
                userView.showMethod(filterCreditCardType(response.body()));
                userView.showProgress(false);
            }

            @Override
            public void onFailure(Call<List<CreditCard>> call, Throwable t) {

            }
        });
    }

    public List<CreditCard> filterCreditCardType(List<CreditCard> list) {
        List<CreditCard> onlyCreditList = new ArrayList<>();
        for (CreditCard card : list) {
            if (card.isCreditCard())
                onlyCreditList.add(card);
        }
        return onlyCreditList;
    }
}
