package meli.com.checkoutmp;

import meli.com.checkoutmp.data.ApiMercadoPago;
import meli.com.checkoutmp.data.ServiceApi;

/**
 * Created by diegoiribarne on 9/6/16.
 */
public class Injection {
    public static ServiceApi provideServiceApi() {
        return ApiMercadoPago.getInstance();
    }
}
